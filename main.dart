
// import 'dart:js';

import 'package:flutter/material.dart';
import 'home.dart';
import 'choose_location.dart';
import 'loading.dart';
// import 'dart:js';
// import ' package:first_flutter_project/main.dart => dart:js';
void main() => runApp(MaterialApp(
  initialRoute: '/home',
  routes: {
    '/': (context) => Loading(),
    '/home': (context) => Home(),
    '/location': (context) => ChooseLocation(),
  },
));




